## -*- mode: Makefile -*-
##
## Copyright (c) 2012, 2013, 2014 The University of Utah
##
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License as
## published by the Free Software Foundation; either version 2 of
## the License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

###############################################################################

SRCDIR		= @srcdir@
TOP_SRCDIR	= @top_srcdir@
SUBDIR		= tests
OBJDIR		= @top_builddir@

SUBDIRS = dwdebug

include $(OBJDIR)/Makeconf

CFLAGS += -I$(TOP_SRCDIR)/target -I$(OBJDIR) -I$(TOP_SRCDIR)/lib \
	-I$(TOP_SRCDIR)/include -I$(TOP_SRCDIR)/dwdebug 
CFLAGS += $(ELFUTILS_CFLAGS) $(GLIB_CFLAGS)
ifeq ($(ENABLE_XENACCESS),"1")
	CFLAGS += -D ENABLE_XENACCESS $(XENACCESS_FLAGS)
	CFLAGS += -I$(XENACCESS_INC)
endif
ifeq ($(ENABLE_DISTORM),"1")
	CFLAGS += -D ENABLE_DISTORM -D SUPPORT_64BIT_OFFSET 
	CFLAGS += -I$(DISTORM)/include
endif

LDFLAGS += $(ELFUTILS_LDFLAGS) $(GLIB_LDFLAGS)
ifeq ($(ENABLE_XENACCESS),"1")
	LDFLAGS += -lxenctrl -lxenstore -lc -L$(XENACCESS_LIBDIR)
endif
ifeq ($(ENABLE_DISTORM),"1")
	LDFLAGS += -L$(DISTORM)/lib -ldistorm3
endif
LDFLAGS += -lpthread

HEADERS := $(TOP_SRCDIR)/dwdebug/dwdebug.h \
	$(TOP_SRCDIR)/target/target.h $(TOP_SRCDIR)/target/target_api.h \
	$(TOP_SRCDIR)/target/probe.h $(TOP_SRCDIR)/target/probe_api.h \
	$(TOP_SRCDIR)/include/list.h $(TOP_SRCDIR)/include/alist.h \
	$(TOP_SRCDIR)/include/common.h $(TOP_SRCDIR)/include/log.h \
	$(TOP_SRCDIR)/include/waitpipe.h $(TOP_SRCDIR)/include/evloop.h \
	$(TOP_SRCDIR)/include/monitor.h \
	$(TOP_SRCDIR)/include/output.h $(TOP_SRCDIR)/include/clfit.h \
	$(TOP_SRCDIR)/include/regcache.h $(TOP_SRCDIR)/include/arch.h

LIBRARIES := libvmitest.la

OBJECTS := monitor_dummy.lo

TESTS := clrange clrangesimple clmatch clmatchone evloop_waitpipe \
	monitor_threads monitor_processes monitored_dummy_child regcache
TESTOBJECTS := $(addsuffix .lo,$(TESTS))

ASSISTANTS := dummy dummy.threads threads.leader.exit dlopen
ifeq ("$(shell uname -m)","x86_64")
    ASSISTANTS += regval_x86_64
else
    ASSISTANTS += regval_x86
endif
TESTOBJECTS += $(addsuffix .lo,$(ASSISTANTS))

STATICLIBS := $(OBJDIR)/dwdebug/libdwdebug.la $(OBJDIR)/lib/libvmilib.la
#	 $(OBJDIR)/target/libtarget.la
ifeq ($(ENABLE_XENACCESS),"1")
	STATICLIBS += $(XENACCESS_A)
endif
ifeq ($(ENABLE_LIBVMI),"1")
	STATICLIBS += $(LIBVMI)/lib/libvmi.a
endif

# Get libtool to shut up when compiling.
RM=rm -f

all:	$(LIBRARIES) $(ASSISTANTS) $(TESTOBJECTS) $(TESTS) all-subdirs

include $(TOP_SRCDIR)/Makerules

.c.lo:	$(HEADERS) $(STATICLIBS)
	@$(LIBTOOL) --tag=CC --mode=compile ${CC} ${CFLAGS} -c $<

libvmitest.la:	$(HEADERS) $(OBJECTS)
	@$(LIBTOOL) --tag=CC --mode=link $(CC) -o $@ $(OBJECTS)

dummy: dummy.lo 
	@$(LIBTOOL) --tag=CC --mode=link $(CC) $(LT_LDFLAGS) -o $@ $@.lo

dummy.threads: dummy.threads.lo
	@$(LIBTOOL) --tag=CC --mode=link $(CC) $(LT_LDFLAGS) -o $@ $@.lo \
		-pthread
dlopen: dlopen.lo 
	@$(LIBTOOL) --tag=CC --mode=link $(CC) $(LT_LDFLAGS) -o $@ $@.lo \
		-ldl

threads.leader.exit: threads.leader.exit.lo
	@$(LIBTOOL) --tag=CC --mode=link $(CC) $(LT_LDFLAGS) -o $@ $@.lo \
		-pthread

clrange: $(HEADERS) $(LIBRARIES) clrange.lo $(STATICLIBS)
	@$(LIBTOOL) --tag=CC --mode=link $(CC) $(LT_LDFLAGS) -o $@ $@.lo \
		$(CFLAGS) $(LIBRARIES) $(STATICLIBS) $(LDFLAGS)

clrangesimple: $(HEADERS) $(LIBRARIES) clrangesimple.lo $(STATICLIBS)
	@$(LIBTOOL) --tag=CC --mode=link $(CC) $(LT_LDFLAGS) -o $@ $@.lo \
		$(CFLAGS) $(LIBRARIES) $(STATICLIBS) $(LDFLAGS)

clmatch: $(HEADERS) $(LIBRARIES) clmatch.lo $(STATICLIBS)
	@$(LIBTOOL) --tag=CC --mode=link $(CC) $(LT_LDFLAGS) -o $@ $@.lo \
		$(CFLAGS) $(LIBRARIES) $(STATICLIBS) $(LDFLAGS)

clmatchone: $(HEADERS) $(LIBRARIES) clmatchone.lo $(STATICLIBS)
	@$(LIBTOOL) --tag=CC --mode=link $(CC) $(LT_LDFLAGS) -o $@ $@.lo \
		$(CFLAGS) $(LIBRARIES) $(STATICLIBS) $(LDFLAGS)

evloop_waitpipe: $(HEADERS) $(LIBRARIES) evloop_waitpipe.lo $(STATICLIBS)
	@$(LIBTOOL) --tag=CC --mode=link $(CC) $(LT_LDFLAGS) -o $@ $@.lo \
		$(CFLAGS) $(LIBRARIES) $(STATICLIBS) $(LDFLAGS)

monitor_threads: $(HEADERS) $(LIBRARIES) monitor_threads.lo $(STATICLIBS)
	@$(LIBTOOL) --tag=CC --mode=link $(CC) $(LT_LDFLAGS) -o $@ $@.lo \
		$(CFLAGS) $(LIBRARIES) $(STATICLIBS) $(LDFLAGS)

monitor_processes: $(HEADERS) $(LIBRARIES) monitor_processes.lo $(STATICLIBS)
	@$(LIBTOOL) --tag=CC --mode=link $(CC) $(LT_LDFLAGS) -o $@ $@.lo \
		$(CFLAGS) $(LIBRARIES) $(STATICLIBS) $(LDFLAGS)

monitored_dummy_child: $(HEADERS) $(LIBRARIES) monitored_dummy_child.lo $(STATICLIBS)
	@$(LIBTOOL) --tag=CC --mode=link $(CC) $(LT_LDFLAGS) -o $@ $@.lo \
		$(CFLAGS) $(LIBRARIES) $(STATICLIBS) $(LDFLAGS)

regcache: $(HEADERS) $(LIBRARIES) regcache.lo $(STATICLIBS)
	@$(LIBTOOL) --tag=CC --mode=link $(CC) $(LT_LDFLAGS) -o $@ $@.lo \
		$(CFLAGS) $(LIBRARIES) $(STATICLIBS) $(LDFLAGS)

regval_x86: $(HEADERS) $(LIBRARIES) regval_x86.lo $(STATICLIBS)
	@$(LIBTOOL) --tag=CC --mode=link $(CC) $(LT_LDFLAGS) -o $@ $@.lo \
		$(CFLAGS) $(LIBRARIES) $(STATICLIBS) $(LDFLAGS)

regval_x86_64: $(HEADERS) $(LIBRARIES) regval_x86_64.lo $(STATICLIBS)
	@$(LIBTOOL) --tag=CC --mode=link $(CC) $(LT_LDFLAGS) -o $@ $@.lo \
		$(CFLAGS) $(LIBRARIES) $(STATICLIBS) $(LDFLAGS)

test:	$(TESTS) test-subdirs
	./clrange
	./clrangesimple
	./clmatch
	./clmatchone
	./evloop_waitpipe
	./monitor_threads
	./monitor_processes
	./regcache

install: def-install install-subdirs

clean: clean-subdirs
	@$(LIBTOOL) --mode=clean rm -f $(OBJECTS) $(LIBRARIES) $(ASSISTANTS) $(TESTS) $(TESTOBJECTS) 
	$(RM) -rf .libs

distclean: distclean-subdirs

# How to recursively descend into subdirectories to make general
# targets such as `all'.
%.MAKE:
	@$(MAKE) -C $(dir $@) $(basename $(notdir $@))
%-subdirs: $(addsuffix /%.MAKE,$(SUBDIRS)) ;

.PHONY:	$(SUBDIRS)

.SECONDARY:
